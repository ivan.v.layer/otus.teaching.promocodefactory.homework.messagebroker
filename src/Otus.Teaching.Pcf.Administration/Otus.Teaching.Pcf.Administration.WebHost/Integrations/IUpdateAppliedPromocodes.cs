﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Integrations
{
    public interface IUpdateAppliedPromocodes
    {
        Task UpdateAppliedPromocodesAsync(Guid id);
    }
}