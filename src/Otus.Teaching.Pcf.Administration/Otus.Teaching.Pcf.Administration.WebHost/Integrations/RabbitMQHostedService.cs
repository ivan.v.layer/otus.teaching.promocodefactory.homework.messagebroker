﻿//using Castle.Core.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using SQLitePCL;
using System;
using System.Reflection.Metadata;
using System.Text;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;



namespace Otus.Teaching.Pcf.Administration.WebHost.Integrations
{
    public class RabbitMQHostedService : BackgroundService
    {

        ILogger<RabbitMQHostedService> _log;
        private IConnection _connection;
        private IModel _channel;

        private readonly IServiceProvider _serviceProvider;

        public IConfiguration Configuration;



        public RabbitMQHostedService(ILogger<RabbitMQHostedService> log, IServiceProvider serviceProvider, IConfiguration configuration)
        {
            _log = log;
            _serviceProvider = serviceProvider;
            Configuration = configuration;
        }
        public override Task StartAsync(CancellationToken cancellationToken)
        {
            return base.StartAsync(cancellationToken);
        }

        protected override async Task  ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.Delay(5000);

            // Выполняем задачу пока не будет запрошена остановка приложения
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    await DoCheckRabbitMQQueue();
                }
                catch (Exception ex)
                {
                    _log.LogInformation("Error processing RabbitMQ queue: {0}.", ex.Message);
                }

                await Task.Delay(5000);
            }

        }

        protected bool IsConnected()
        {
            if (_connection == null)
            {
                _log.LogInformation("Нет соединения с RabbitMQ");
                return false;

            }

            return true;
        }

        protected void DoConnect()
        {
            if (_connection == null)
            {
                try
                {

                    //Попытка подключиться
                    _log.LogInformation("Start processing RabbitMQ queue.");

                    var factory = new ConnectionFactory { HostName = Configuration["RabbitMQSettings:HostName"], 
                        UserName = Configuration["RabbitMQSettings:UserName"], 
                        Password = Configuration["RabbitMQSettings:Password"] };
                    _connection = factory.CreateConnection();
                    _channel = _connection.CreateModel();
                    _channel.QueueDeclare(queue: Configuration["RabbitMQSettings:queue"], durable: false, exclusive: false, autoDelete: false, arguments: null);

                }
                catch (Exception ex)
                {
                    _log.LogInformation("Error during start processing RabbitMQ queue: {0}.", ex.Message);
                    return;
                }

                if (_channel == null)
                {
                    _log.LogInformation("Очередь не найдена");
                    return;
                }

                var consumer = new EventingBasicConsumer(_channel);

                if (consumer == null)
                {
                    _log.LogInformation($"Не удалось зарегистрировать потребителя");
                    return;
                }

                consumer.Received += async (ch, ea) =>
                {
                    var id = Encoding.UTF8.GetString(ea.Body.ToArray());

                    // обрабатываем полученное сообщение
                    _log.LogInformation($"Получено сообщение: {id}");
                    Guid userGuid;
                    if (Guid.TryParse(id, out userGuid))
                    {
                        using (var scope = _serviceProvider.CreateScope())
                        {
                            var _updateAppliedPromocodes = scope.ServiceProvider.GetRequiredService<IUpdateAppliedPromocodes>();
                            await _updateAppliedPromocodes.UpdateAppliedPromocodesAsync(userGuid);
                        }
                    }
                    else
                    {
                        _log.LogError($"Получен ошибочный GUID id: {id}");
                        return;
                    }
                    _channel.BasicAck(ea.DeliveryTag, false);
                };

                _channel.BasicConsume(Configuration["RabbitMQSettings:routingKey"], false, consumer);
            }

        }

        protected Task DoCheckRabbitMQQueue()
        {
            if (IsConnected() == false)
            {
                DoConnect();
            }


            return Task.CompletedTask;
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            //Действия при остановке сервиса 

            _channel.Close();
            _connection.Close();
            _log.LogInformation("Остановлена обработка очереди.");

            return base.StopAsync(cancellationToken);
        }
    }
}
