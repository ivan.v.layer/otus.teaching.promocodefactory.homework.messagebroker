﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.DataAccess.Repositories;



namespace Otus.Teaching.Pcf.Administration.WebHost.Integrations
{
    public class UpdateAppliedPromocodes : IUpdateAppliedPromocodes
    {

        private readonly IRepository<Employee> _employeeRepository;

        public UpdateAppliedPromocodes(IRepository<Employee> employeeRepository)
        {

            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Обновить количество выданных промокодов
        /// </summary>
        /// <param name="id">Id сотрудника, например <example>451533d5-d8d5-4a11-9c7b-eb9f14e1a32f</example></param>
        /// <returns></returns>
        public async Task UpdateAppliedPromocodesAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
               return;

            employee.AppliedPromocodesCount++;

            await _employeeRepository.UpdateAsync(employee);

        }
    }
}
