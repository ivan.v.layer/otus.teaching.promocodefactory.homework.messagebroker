﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.RabbitMQ
{
    public interface IRabbitMqService
    {
        void SendMessage(object obj);
        Task SendMessage(string message);
    }
}
