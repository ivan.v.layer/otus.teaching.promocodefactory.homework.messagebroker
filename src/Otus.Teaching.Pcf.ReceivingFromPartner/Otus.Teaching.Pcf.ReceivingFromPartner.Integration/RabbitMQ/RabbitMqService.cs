﻿using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using System.Data;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.RabbitMQ
{
    public class RabbitMqService : IRabbitMqService
    {
        public IConfiguration Configuration { get; }
        public RabbitMqService(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void SendMessage(object obj)
        {
            var message = JsonSerializer.Serialize(obj);
            SendMessage(message);

        }
        public Task SendMessage(string message)
        {

            var factory = new ConnectionFactory() { HostName = Configuration["RabbitMQSettings:HostName"], 
                UserName = Configuration["RabbitMQSettings:UserName"], 
                Password = Configuration["RabbitMQSettings:Password"]};
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: Configuration["RabbitMQSettings:queue"],
                               durable: false,
                               exclusive: false,
                               autoDelete: false,
                               arguments: null);

                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: "",
                               routingKey: Configuration["RabbitMQSettings:routingKey"],
                               basicProperties: null,
                               body: body);
            }

            return Task.CompletedTask;
        }
    }
}


