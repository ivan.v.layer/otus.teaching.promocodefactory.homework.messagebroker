﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.RabbitMQ;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class AdministrationGateway
        : IAdministrationGateway
    {
        private readonly IRabbitMqService _rabbitMQ;

        public AdministrationGateway(IRabbitMqService rabbitMQ)
        {
            _rabbitMQ = rabbitMQ;
        }
        
        public async Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId)
        {
            await _rabbitMQ.SendMessage(partnerManagerId.ToString());
        }
    }
}